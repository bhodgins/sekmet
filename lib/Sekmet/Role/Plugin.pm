package Sekmet::Role::Plugin;

use Moose::Role;

has 'name' => (
    is     => 'rw',
    isa    => 'Str',
);

has 'core' => (
    is     => 'ro',
    isa    => 'Sekmet',
);


sub initialize {
    # Default: do nothing.
}


1;
