# An API to the core:
package Sekmet::API;


use Moose::Role;


has '_core' => (
    does    => 'Sekmet',
    is      => 'ro',
    handles => 'Sekmet::API',
);

# Basic stuff:
sub load_plugin  {}
sub dispatch     {}
sub plugin_named {}
sub plugins_with {}
sub protocol     {}

1;
