package Sekmet::Line;

use Moose;

with 'Reflex::Role::Collectible';
with 'Sekmet::API';
extends 'Reflex::Base';


has 'i_active'    => (
    isa         => 'Bool',
    is          => 'rw',
    default     => 1,
);

has 'c_active'    => (
    isa         => 'Bool',
    is          => 'rw',
    default     => 1,
);

has 'initiator' => (
    isa         => 'FileHandle',
    is          => 'ro',
    required    => 1,
);

has 'connector' => (
    isa         => 'FileHandle',
    is          => 'rw',
);

with 'Reflex::Role::Streaming' => {
    att_active    => 'i_active',
    att_handle    => 'initiator',
};


sub BUILD    {}
sub DEMOLISH {}

sub on_initiator_data {
    my ($self, $data) = @_; 
    my $message = $data->octets;

    # SENT protocol is asynchronious to anything else on the line so instead of
    # using SENT as a filter protocol information is sent to sent for parsing.
    # Flow is then continued if possible. HOWEVER, all SENT protocol commands
    # start with an at (@) character, followed by a space:
    if ($message =~ /^@\s/ ) {

	$data->{line} = $self;
	$self->protocol->parse($data);
    }

    # Relay, if possible:
    $self->put_connector($data) if $self->connector;
    
    # An almost confusing decision is, should event dispatching happen before
    # or after the main protocol handling? Keep in mind, this order may change.
    $self->dispatch('LineHandler', 'on_initiator_data', {line => $self, data => $data});
}

# Same thing as on_initiator_data, but flow is backwards:
sub on_connector_data {
    my ($self, $data) = @_;
    my $message = $data->octets;

    if ($message =~ /^@\s/ ) {
	
	$data->{line} = $self;
	$self->protocol->parse($data);
    }

    $self->put_initiator($data);
    $self->dispatch('LineHandler', 'on_initiator_data', {line => $self, data => $data});
}

sub on_initiator_closed {
    my $self = shift;

    if ($self->connector) {
	my $message = $self->protocol->construct('HUP', 'Remote host closed connection');
	
	$self->put_connector($message);
    }
}

sub on_connector_closed {
    my $self = shift;

    if ($self->initiator) {
	my $message = $self->protocol->construct('HUP', 'Remote host closed connection');

	$self->put_initiator($message);
    }
}

sub on_initiator_error {
    my $self = shift;

    $self->multicast($self->protocol->construct('HUP', 'Internal proxy error on local end.'));
}

sub on_connector_error {
    my $self = shift;

    $self->multicast($self->protocol->construct('HUP', 'Internal proxy error on remote end.'));
}

# Send a message to the initiator, and if applicable, the connector:
sub multicast {
    my ($self, $data) = @_;

    $data .= "\n";
    print $data . "\n";
    $self->put_initiator($data);
    $self->put_connector($data) if $self->connector;
}


__PACKAGE__->meta->make_immutable;
