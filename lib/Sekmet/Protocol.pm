package Sekmet::Protocol;

use Moose;
use YAML::Tiny;


use constant {
    MESSAGE_HUP     => 'HUP',
    MESSAGE_DIAL    => 'DIAL',
    MESSAGE_HOLD    => 'HOLD',
};


# A security hack to prevent people from calling stuff they shouldn't. This must
# be lowercase since it is checked after case insensitivity adjustments:
my @acceptable = qw(dial hold hup);


sub parse {
    my ($self, $data) = @_;
    
    # Messages in SENT are always prefixed by an at (@) symbol, and a following
    # space. The original protocol prototype just used an at symbol. I find that
    # a following space is exponentially more rare, and it may help to make SENT
    # even more human readable, which is one of its primary goals:
    return unless $data->octets =~ /^@\s/;
    my $message = substr($data->octets, 2);

    # SENT will ignore anything that it doesn't understand, but if you want to
    # throw out a bunch of comments, the best way to do it is to use a
    # headerless workload. for example: @ :HEllo, World! could be used as a
    # useful comment:
    return if $message =~ /^:/;

    # In SENT protocol, we seperate the workload from the command and
    # parameters which make up a SENT the same way PRIVMSG in IRC protocol does
    # by prefixing the workload with a colon (:):
    my ($header, $workload) = split /:/, $message;

    # Strict, but do not accept messages with workloads if 81 characters or
    # more. The 80 character limit is to help with flooding as well as allow
    # a full length message to fit properly in a 80x?? terminal:
    if ($workload) {

	return if length($workload) > 80;
    }

    # Now that we have the workload out of the way, the command and its parameters are seperated by spaces, just like most messages in IRC protocol:
    $header =~ s/^\s+|\s+$//;         # trim
    my @params  = split / /, $header; # build an array of parameters
    my $command = lc(shift @params);  # lc for case insensitivity
    
    chomp $command; # remove that pesky \n!

    # We have everything we need to handle a SENT message:
    if ($self->can($command) && grep $command, @acceptable) {
	$self->$ {\ $command}($data, $workload, @params); # Return.
    }
}


sub dial {
    my ($self, $data, $workload, $number) = @_;
    return unless @_ == 4; # We need three arguments!

    # I expect anyone to make their clients eliminate anything that is not a
    # number when handling telecom numbers, and everything that isn't a period
    # or alpha character when performing a DNS lookup.
    $number =~ s/[^\w\d.]+//g;
    print "$number\n";

    # Sekmet supports connection via two methods: hostname, and telecom number.
    # to use a hostname, a hostmap file is read. If no matching hostmap entry
    # is found, then the hostname is transferred to the next higher level tier.
    # If the hostname is not resolved when at a top level tier (a proxy
    # exchange without any default routes), then the proxy will return NXDOMAIN
    # (non existant) and HUP (hangup):
    if ($number =~ /[\w.]+/) {

	# TODO - not implemented yet.
    }

    # We can assume by now that we are dialing a number.
    
    # SENT signalling protocol requires that if there is a leading zero, the
    # connection must be transferred to the next higher level tier. To do this,
    # we strip off (1) leading zero, and make the connection. Sekmet in
    # particular will actually use Geo::IP to attempt to find the closest
    # available proxy:
    if ($number =~ /^0/) {

	my $routing;
	unless ($routing = YAML::Tiny->read('etc/routing.yaml')) {

	    # Hang up:
	    hup($data, 'Internal server error. Please contact the admin.');
	}
	
	
	# $data->{line}->connection()
    }
}


sub hup {
    my ($self, $data, $workload) = @_;

    my $message = $self->construct(MESSAGE_HUP, $workload);
    $message; # Return.
}


sub hold {

}


# Helper function for building SENT messages:
sub construct {
    my ($self, $command, $workload, @params) = @_;
    return unless @_ >= 1;

    # Prepare the message with a prefix:
    my $message = "@ $command";

    # join parameters into a string seperated by spaces:
	$message .= " @{ [join ' ', @params] }" if @params;
    
    # Append a workload if necessary:
    $message .= " :$workload" if $workload;

    $message; # Return.
}

__PACKAGE__->meta->make_immutable;
