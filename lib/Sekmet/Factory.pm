package Sekmet::Factory;

use Moose;
use Reflex::Collection;
use Moose::Autobox;
use Sekmet::Line;

with 'Sekmet::API';
extends 'Reflex::Acceptor';


sub on_accept {
    my ($self, $socket) = @_;

    $self->_core->lines->push(Sekmet::Line->new( _core => $self->_core, initiator => $socket->{handle}));
    $self->dispatch('FactoryWatchman', 'on_factory_accept', $socket);
}

sub on_error {
    my ($self, $error) = @_;

    warn $error->formatted(), "\n";
    $self->dispatch('FactoryWatchman', 'on_factory_error', $error);
    $self->stop();
}

__PACKAGE__->meta->make_immutable;
