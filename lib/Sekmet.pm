package Sekmet;

use Moose;
use Moose::Autobox;
use Class::Load 'try_load_class';
use List::Util 'first';
use Reflex::Collection;
use IO::Socket::INET;
use Sekmet::Factory;
use Sekmet::Protocol;
use Die::Hard;

with 'MooseX::SimpleConfig';
extends 'Reflex::Base';

has '+configfile' => ( default => 'etc/sekmet.yaml' );

# Look at Moose::Manual!:
has 'plugins'  => (
    isa        => 'ArrayRef[Sekmet::Plugin]',
    is         => 'rw',
    default    => sub { [] },
);

has 'lines'    => (
    isa        => 'ArrayRef[Sekmet::Line]',
    is         => 'rw',
    default    => sub { [] },
);

has 'factory'  => (
    isa        => 'Sekmet::Factory',
    is         => 'rw',
);

has 'protocol' => (
    does       => 'Sekmet::Protocol',
    is         => 'ro',
    default    => sub {
	Sekmet::Protocol->new( _core => shift );
	#my $proto = Sekmet::Protocol->new( _core => shift );
	#Die::Hard->new($proto);
    },
    lazy       => 1,
);

# Called before new(), with parameters to the constructor passed. This prevents
# the need for coercion when it is not quite absolutely necessary:
sub BUILDARGS {
    my $class = shift;
    my %data = @_;
    my @plugins;

    # Load plugins listed in the config file:
    foreach my $plugin ( @{ $data{plugins} } ) {

	# It would be kind of nice if I could get rid of the duplicate code
	# here. The thing is, the class is not blessed yet here, so possibly
	# fix load_plugin to return the loaded plugin as well, and use it to
	# build an array reference and send that to the constructor?:
	print "loading plugin '$plugin'\n";
	require $plugin;
	my $obj = $plugin->new(delete $data{plugins}{plugin});
	push @plugins, $obj;
    }

    # We should delete the plugins hashref to prevent plugins from being sent
    # to the constructor:
    delete $data{plugins}; # clean up some more
    $data{plugins} = \@plugins if @plugins;

    # new() still needs constructor parameters, since we intercepted them:
    return \%data;
}


# After new(), we need to fire up the acceptor:
sub BUILD {
    my $self = shift;
    
    # Create the socket for Sekmet::Factory:
    my $so_acceptor = IO::Socket::INET->new(
	LocalAddr    => '127.0.0.1',
	LocalPort    => 8080,
	Listen       => 5,
	Reuse        => 1,
    );
    
    # Bless the factory:
    $self->factory( Sekmet::Factory->new( _core => $self, listener => $so_acceptor) );
}


## A Plugin system inspired by Dist::Zilla ##:
sub plugins_with {
        my ($self, $role) = @_;
    
	$self->plugins->grep( sub { $_->does($role) } );
}


# Post blessed plugin loading:
sub load_plugin {
    my ($self, $plugin, @args) = @_;

    print "loading plugin '$plugin'\n";
    my ($success, $error) = try_load_class($plugin);
    if (!$success) {
        return warn "the plugin: '$plugin' would not load:\n $error\n";
    }

    # I stole the following from my MUD client project, Darkmist. It is a not
    # quite foolproof, yet helpful way of pushing plugin developers to use a
    # compatible API. It is important for plugins and their core to understand
    # eachother, otherwise things can break, especially between versions:
    $plugin->does('Sekmet::Role::Plugin') or return warn
        "cannot load plugin '$plugin' because all plugins must consume ",
	"Darkmist::Role::Plugin. If you did not write this module and $plugin ",
	"is really a plugin for Darkmist, then please contact its author with ",
	"a friendly little whack.\n";

    # And someone once said, "Let there be light!":
    $plugin = $plugin->new( _core => $self, @args );
    $self->plugins->push($plugin);

    # Please use init() instead of BUILD for plugin initialization when writing
    # plugins. I had a very good reason when I was last actively developing
    # Darkmist, but in all honesty I have no idea what it was at the time of
    # this writing. The API will provide a dummy init(), so it is not required.

    $plugin->init;
}


# shorthand event dispatch helper function for plugins:
sub dispatch {
    my ($self, $scope, $call, @args) = @_;

    # Basically we're just trying to find plugins that can handle said event.
    # We call a function, and pass some @args, and we're done. The only
    # downside which may be a good thing is that there are no return values:
    my @plugins = $self->plugins_with($scope);
    foreach my $plugin (@plugins) {

	if ($plugin->can($call)) {

	    $plugin->$ {\ $call}(@args);
	}
    }
}


# I use this sparingly, because it can cause problems with updates, migration,
# dependancy, and a whole lot of other things but I provide it here anyways:
sub plugin_named {
    my ($self, $name) = @_;

    first { $_->name eq $name } $self->plugins->flatten;
}


__PACKAGE__->meta->make_immutable;
